Source: rust-csv-core
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 15),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-memchr-2+libc-dev <!nocheck>,
 librust-memchr-2-dev <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Paride Legovini <pl@ninthfloor.org>
Standards-Version: 4.2.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/csv-core]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/csv-core
Homepage: https://github.com/BurntSushi/rust-csv

Package: librust-csv-core-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-memchr-2-dev
Recommends:
 librust-csv-core+libc-dev (= ${binary:Version})
Provides:
 librust-csv-core-0-dev (= ${binary:Version}),
 librust-csv-core-0.1-dev (= ${binary:Version}),
 librust-csv-core-0.1.6-dev (= ${binary:Version})
Description: Bare bones CSV parsing with no_std support - Rust source code
 This package contains the source for the Rust csv-core crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-csv-core+libc-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-csv-core-dev (= ${binary:Version}),
 librust-memchr-2+libc-dev
Provides:
 librust-csv-core+default-dev (= ${binary:Version}),
 librust-csv-core-0+libc-dev (= ${binary:Version}),
 librust-csv-core-0+default-dev (= ${binary:Version}),
 librust-csv-core-0.1+libc-dev (= ${binary:Version}),
 librust-csv-core-0.1+default-dev (= ${binary:Version}),
 librust-csv-core-0.1.6+libc-dev (= ${binary:Version}),
 librust-csv-core-0.1.6+default-dev (= ${binary:Version})
Description: Bare bones CSV parsing with no_std support - feature "libc"
 This metapackage enables feature libc for the Rust csv-core crate, by pulling
 in any additional dependencies needed by that feature.
